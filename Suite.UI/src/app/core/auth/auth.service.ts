import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { SettingsService } from '../settings/settings.service';

@Injectable()
export class AuthService {
    public token: string;
    private storageKey = 'currentUser';

    public constructor(
        private router: Router,
        private http: Http,
        private settings: SettingsService) {
            // set token if saved in local storage
            const currentUser = this.getCurrentUser();
            this.token = currentUser && currentUser.token;
        }

    public getCurrentUser(): any {
        const currentUser = localStorage.getItem(this.storageKey);
        if (currentUser) {
            return JSON.parse(currentUser);
        }
        return currentUser;
    }

    public getLoggedUser(): any {
        const user = this.getCurrentUser();
        if (user) {
            return user;
        }
        this.router.navigate(['/login']);
        return user;
    }

    public logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem(this.storageKey);
        this.router.navigate(['/login']);
    }

    public isLoggedIn(): boolean {
        const currentUser = this.getCurrentUser();
        if (currentUser) {
            const expiration = new Date(currentUser.expiration);
            const now = new Date();
            if (now.getTime() < expiration.getTime()) {
                return true;
            }
        }
        return false;
    }

    // return true: to indicate successful login, false: to indicate failed login
    public login(organization: string, username: string, password: string): Observable<boolean> {
        const args = new URLSearchParams();
        args.append('organization', organization);
        args.append('username', username);
        args.append('password', password);
        const params = args.toString();
        const tokenUrl = this.settings.getAppSetting('apiUrl') + 'token';

        return this.http.post(tokenUrl, params, { headers: this.getLoginHeaders() })
            .map((res: Response) => {
                const data = res.json();
                if (data) {
                    const now = new Date();
                    const expirationDate = new Date(now.getTime() + ((data.expires_in / 60) * 60 * 1000));
                    const currentUser = JSON.stringify(
                    {
                        token: data.access_token,
                        expiration: expirationDate,
                        user: data.user,
                        service: data.service
                    });
                    localStorage.setItem(this.storageKey, currentUser);
                    return true;
                }
                return false;
        });
    }

    private getLoginHeaders(): Headers {
        const headers = new Headers();
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Cache-Control', 'no-cache');
        return headers;
    }
}
