import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

declare var $: any;

@Injectable()
export class SettingsService {

    public user: any;
    public app: any;
    public layout: any;

    constructor(public http: Http) {

        // User Settings
        // -----------------------------------
        this.user = {
            name: 'XXX',
            job: 'YYY',
            picture: 'assets/img/user/user.png'
        };

        // App Settings
        // -----------------------------------
        this.app = {
            name: 'Suite',
            apiUrl: 'http://localhost/', /* ./ */
            description: 'Sistema de Administración',
            year: ((new Date()).getFullYear()),
            title: 'Prescripciones Médicas'
        };

        // Layout Settings
        // -----------------------------------
        this.layout = {
            isFixed: true,
            isCollapsed: false,
            isBoxed: false,
            isRTL: false,
            horizontal: false,
            isFloat: false,
            asideHover: false,
            theme: null,
            asideScrollbar: false,
            isCollapsedText: false,
            useFullLayout: false,
            hiddenFooter: false,
            offsidebarOpen: false,
            asideToggled: false,
            viewAnimation: 'ng-fadeInUp'
        };

        http.get('assets/server/proxy.conf.json')
        .subscribe((data) => {
            const settings = data.json();
            this.app.name = settings.name;
            this.app.apiUrl = settings.apiUrl;
            this.app.description = settings.description;
            if (document) {
                document.title = this.app.name;
            }
        });

    }

    getAppSetting(name) {
        return name ? this.app[name] : this.app;
    }
    getUserSetting(name) {
        return name ? this.user[name] : this.user;
    }
    getLayoutSetting(name) {
        return name ? this.layout[name] : this.layout;
    }

    setAppSetting(name, value) {
        if (typeof this.app[name] !== 'undefined') {
            this.app[name] = value;
        }
    }
    setUserSetting(name, value) {
        if (typeof this.user[name] !== 'undefined') {
            this.user[name] = value;
        }
    }
    setLayoutSetting(name, value) {
        if (typeof this.layout[name] !== 'undefined') {
            return this.layout[name] = value;
        }
    }

    toggleLayoutSetting(name) {
        return this.setLayoutSetting(name, !this.getLayoutSetting(name));
    }
}
