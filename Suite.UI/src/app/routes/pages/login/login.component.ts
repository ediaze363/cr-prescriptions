import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public valForm: FormGroup;
    public message: string;

    constructor(
        public settings: SettingsService,
        private authService: AuthService,
        private router: Router,
        private fb: FormBuilder) {

        this.valForm = fb.group({
            'username': [null, Validators.required],
            'password': [null, Validators.required],
            'organization': ['BLANK', Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        // tslint:disable-next-line:forin
        for (const item in this.valForm.controls) {
            this.valForm.controls[item].markAsTouched();
        }
        if (this.valForm.valid) {
            this.message = '';
            this.authService.login(value.organization, value.username, value.password).subscribe(
            result => {
                    if (result === true) {
                        console.log('Valid login!');
                        this.router.navigate(['/home']);
                    } else {
                        this.message = 'Usuario o contraseña incorrectos.';
                    }
                },
                error => {
                    switch (error.status) {
                        case 0:
                            this.message = 'No se puede conectar con Prescripciones API. Por favor revise que esté corriendo el servicio.';
                            break;
                        case 400:
                        case 403:
                            this.message = 'Usuario o contraseña incorrectos.';
                            break;
                        default:
                            const failure = <any>error;
                            console.error(failure);
                            if (failure._body) {
                                this.message = failure._body;
                            }
                            else {
                                this.message = failure;
                            }
                            break;
                    }
                });
        }
    }

    ngOnInit() {
    }
}
