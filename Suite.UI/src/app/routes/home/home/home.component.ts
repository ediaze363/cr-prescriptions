import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import { AuthService } from '../../../core/auth/auth.service';
import { PrescriptionService } from '../services/prescription.service';
import { Prescription } from '../models/Prescription';

import * as _ from 'lodash';
import { SelectItem } from 'ng2-select';
import { Router } from '@angular/router';
import { Column } from '../../../shared/directives/grid/column';
import { MasterBase } from '../../master/base/master.base.component';
import { NavsearchService } from '../../../layout/header/navsearch/navsearch.service';
import { Patient } from '../models/patient';
import { Dictionary } from '../models/dictionary';
import { SettingsService } from '../../../core/settings/settings.service';
import { ExcelService } from '../services/excel.service';
import * as moment from 'moment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends MasterBase implements OnInit {
    public valForm: FormGroup;
    public isRetrieving = false;
    public docTypes: Array<any> = [];
    public entries: Array<any> = [];
    public docTypeSelected: string;
    public searchDate: string;
    public isSelectedRow = false;
    public selectedId: string;
    public selectedPatient: Patient;
    public titles: Dictionary<string>;

    // Calendar properties
    public userDate: Date = new Date();
    public showDatepicker: boolean;

    constructor(
        protected navService: NavsearchService,
        protected authService: AuthService,
        private service: PrescriptionService,
        private fb: FormBuilder,
        private router: Router,
        public settings: SettingsService,
        private excelService: ExcelService
    ) {
        super(navService, authService);
        this.userDate = this.getCurrentDate();
        const defaultDate = this.getStringDate(this.userDate);
        this.valForm = fb.group({
            'searchDate': [defaultDate, Validators.required],
            'searchPicker': [this.userDate, Validators.required],
            'docType': ['CC', Validators.required],
            'documentId': [null, Validators.required]
        });
        this.columns =
         [
            { title: 'Doc. Paciente', name: 'uri'},
            { title: 'Nombre Paciente', name: 'name' },
            { title: 'Hora', name: 'date' },
            { title: 'Doc. Profesional', name: 'doc'},
            { title: 'Nombre Profesional', name: 'doctor'},
            { title: 'Reg. Profesional', name: 'record'},
            { title: 'Número', name: 'number' }
        ];
        settings.setAppSetting('title', 'Prescripciones');
    }

    public popupCalendar() {
        this.showDatepicker = !this.showDatepicker;
    }
    public onDateChange(date) {
        this.userDate = date;
        const defaultDate = this.getStringDate(this.userDate);
        this.valForm.controls['searchDate'].setValue(defaultDate);
        this.valForm.controls['searchDate'].patchValue(defaultDate);
    }
    private getCurrentDate(): Date {
        const n = new Date();
        n.setDate(n.getDate() - 1);
        const y = n.getFullYear();
        const m = n.getMonth();
        const d = n.getDate();
        return new Date(y, m, d);
    }
    private getStringDate(n): string {
        const y = n.getFullYear();
        const month: number = n.getMonth() + 1;
        const m = month < 10 ? '0' + month : month;
        const d = n.getDate() < 10 ? '0' + n.getDate() : n.getDate();
        return y + '-' + m + '-' + d;
    }
    ngOnInit() {
        super.init();
    }

    public getList(orgId: string) {
        super.getList(orgId);
        this.onChangeTable(this.config);
        this.service.getLabels('TIPO_DOCUMENTO_IDEN').subscribe(
            items => {
                this.docTypes = items;
                this.docTypeSelected = this.docTypes[0];
            },
            error =>  {
                this.message = <any>error;
            }
        );
        this.service.getLabels('LABEL_ATRIBUTOS').subscribe(
            items => {
                const headerLabels: Dictionary<string> = {};
                for (let index = 0; index < items.length; index++) {
                    const key = items[index].id.toLowerCase();
                    headerLabels[key] = items[index].text;
                }
                this.titles = headerLabels;
            },
            error =>  {
                this.message = <any>error;
            }
        );
    }

    public find($ev, input: any) {
        this.isRetrieving = false;
        this.message = '';
        this.isQueried = false;

        $ev.preventDefault();
        // tslint:disable-next-line:forin
        for (const item in this.valForm.controls) {
            this.valForm.controls[item].markAsTouched();
        }
        if (input.searchDate) {
            this.isRetrieving = true;
            this.searchDate = input.searchDate;
            this.service.getInfo(input.searchDate, this.docTypeSelected, input.documentId)
            .subscribe(
            items => {
                this.entries = items;
                const values = [];
                for (let index = 0; index < items.length; index++) {
                    const item = items[index].prescripcion;
                    const docPac = `${item['tipoIDPaciente']}-${item['nroIDPaciente']}`;
                    const nomPac = `${item['paPaciente']} ${item['saPaciente']}, ${item['pnPaciente']} ${item['snPaciente']}`;
                    const hora = `${item['hPrescripcion']}`;
                    const docProf = `${item['tipoIDProf']}-${item['numIDProf']}`;
                    const nomProf = `${item['paProfS']} ${item['saProfS']}, ${item['pnProfS']} ${item['snProfS']}`;
                    const regProf = `${item['regProfS']}`;
                    const numDoc = `${item['noPrescripcion']}`;

                    values.push(
                    {
                        id: docPac,
                        uri: `<a href='#'>${docPac}</a>`,
                        name: nomPac,
                        date: hora,
                        doc: docProf,
                        doctor: nomProf,
                        record: regProf,
                        number: numDoc
                    });
                }
                this.items = values;
                this.isRetrieving = false;
                this.isQueried = true;
                this.onChangeTable(this.config);
            },
            error =>  {
                this.message = <any>error;
                this.isRetrieving = false;
            });
        }
    }

    public selected(input: SelectItem): void {
        this.docTypeSelected = input.id;
    }

    public typed(input: string): void {
        this.docTypeSelected = input;
    }

    public goDetail(data: any): any {
        if (data.column === 'uri') {
            this.isSelectedRow = true;
            this.selectedId = data.row.id;
            const selected = this.entries.filter(item => item.prescripcion.noPrescripcion.indexOf(data.row.number) !== -1);
            if (selected) {
                this.selectedPatient = <Patient>selected[0];
            }
        }
    }

    public goBack(): void {
        this.selectedId = null;
        this.isSelectedRow = false;
    }

    public loadChilds(inputLines: any[], outputLines: any[], parentId?: string, parentField?: string): void {
        for (let index = 0; index < inputLines.length; index++) {
            const line = inputLines[index];
            const feature = {};
            if (parentId) {
                feature[parentField] = parentId;
            }
            // tslint:disable-next-line:forin
            for (const attribute in line) {
                let value = line[attribute];
                const fieldId = attribute.toLowerCase();
                const fieldName = (this.titles[fieldId]) ? this.titles[fieldId] : attribute;
                if (value instanceof Array) {
                    const innerChilds = value;
                    const innerLines = [];
                    for (let inner = 0; inner < innerChilds.length; inner++) {
                        const child = innerChilds[inner];
                        const values = [];
                        // tslint:disable-next-line:forin
                        for (const field in child) {
                            value = child[field];
                            const childId = field.toLowerCase();
                            const childName = (this.titles[childId]) ? this.titles[childId] : field;
                            values.push(`${childName}: ${value}`)
                        }
                        value = values.join(', ');
                        innerLines.push(value);
                    }
                    value = innerLines.join('; ');
                }
                feature[fieldName] = value;
            }
            outputLines.push(feature);
        }
    }

    public exportToExcel(): void {
        this.isRetrieving = true;
        const data: Dictionary<any[]> = {};
        const prescriptions = [];
        const medicines = [];
        const procedures = [];
        const devices = [];
        const nutritionalProducts = [];
        const complementaryServices = [];

        for (let index = 0; index < this.entries.length; index++) {
            let parentId: string = null;
            let parentField: string = null;
            const entry = this.entries[index];
            const prescription = entry.prescripcion;
            const feature = {};
            // tslint:disable-next-line:forin
            for (const attribute in prescription) {
                const value = prescription[attribute];
                const fieldId = attribute.toLowerCase();
                const fieldName = (this.titles[fieldId]) ? this.titles[fieldId] : attribute;
                feature[fieldName] = value;
                if (parentId === null) {
                    parentId = value;
                    parentField = fieldName;
                }
            }
            prescriptions.push(feature);
            this.loadChilds(entry.medicamentos, medicines, parentId, parentField);
            this.loadChilds(entry.procedimientos, procedures, parentId, parentField);
            this.loadChilds(entry.productosnutricionales, nutritionalProducts, parentId, parentField);
            this.loadChilds(entry.dispositivos, devices, parentId, parentField);
            this.loadChilds(entry.serviciosComplementarios, complementaryServices, parentId, parentField);
        }
        data['prescripciones'] = prescriptions;
        if (medicines.length > 0) {
            data['medicamentos'] = medicines;
        }
        if (procedures.length > 0) {
            data['procedimientos'] = procedures;
        }
        if (nutritionalProducts.length > 0) {
            data['productosnutricionales'] = nutritionalProducts;
        }
        if (devices.length > 0) {
            data['dispositivos'] = devices;
        }
        if (complementaryServices.length > 0) {
            data['serviciosComplementarios'] = complementaryServices;
        }
        this.excelService.exportAsExcel(data, 'prescripciones', 'Prescripciones');
        this.isRetrieving = false;
    }
}
