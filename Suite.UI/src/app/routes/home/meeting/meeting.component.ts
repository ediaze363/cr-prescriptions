import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import { AuthService } from '../../../core/auth/auth.service';
import { PrescriptionService } from '../services/prescription.service';
import { Prescription } from '../models/Prescription';

import * as _ from 'lodash';
import { SelectItem } from 'ng2-select';
import { Router } from '@angular/router';
import { Column } from '../../../shared/directives/grid/column';
import { MasterBase } from '../../master/base/master.base.component';
import { NavsearchService } from '../../../layout/header/navsearch/navsearch.service';
import { Patient } from '../models/patient';
import { Dictionary } from '../models/dictionary';
import { SettingsService } from '../../../core/settings/settings.service';
import { ExcelService } from '../services/excel.service';

@Component({
    selector: 'app-meeting',
    templateUrl: './meeting.component.html',
    styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent extends MasterBase implements OnInit {
    public valForm: FormGroup;
    public isRetrieving = false;
    public docTypes: Array<any> = [];
    public entries: Array<any> = [];
    public docTypeSelected: string;
    public searchDate: string;
    public titles: Dictionary<string>;

    // Calendar properties
    public userDate: Date = new Date();
    public showDatepicker: boolean;

    constructor(
        protected navService: NavsearchService,
        protected authService: AuthService,
        private service: PrescriptionService,
        private fb: FormBuilder,
        private router: Router,
        public settings: SettingsService,
        private excelService: ExcelService
    ) {
        super(navService, authService);
        this.userDate = this.getCurrentDate();
        const defaultDate = this.getStringDate(this.userDate);
        this.valForm = fb.group({
            'searchDate': [defaultDate, Validators.required],
            'searchPicker': [this.userDate, Validators.required],
            'docType': ['CC', Validators.required],
            'documentId': [null, Validators.required]
        });
        this.columns =
        [
            { title: 'Estado de la Junta de Profesionales para la prescripción', name: 'info'}
        ];
        settings.setAppSetting('title', 'Junta Profesionales');
    }

    public popupCalendar() {
        this.showDatepicker = !this.showDatepicker;
    }
    public onDateChange(date) {
        this.userDate = date;
        const defaultDate = this.getStringDate(this.userDate);
        this.valForm.controls['searchDate'].setValue(defaultDate);
        this.valForm.controls['searchDate'].patchValue(defaultDate);
    }
    private getCurrentDate(): Date {
        const n = new Date();
        n.setDate(n.getDate() - 1);
        const y = n.getFullYear();
        const m = n.getMonth();
        const d = n.getDate();
        return new Date(y, m, d);
    }
    private getStringDate(n): string {
        const y = n.getFullYear();
        const month: number = n.getMonth() + 1;
        const m = month < 10 ? '0' + month : month;
        const d = n.getDate() < 10 ? '0' + n.getDate() : n.getDate();
        return y + '-' + m + '-' + d;
    }
    ngOnInit() {
        super.init();
    }

    public getList(orgId: string) {
        super.getList(orgId);
        this.onChangeTable(this.config);
        this.service.getLabels('TIPO_DOCUMENTO_IDEN').subscribe(
            items => {
                this.docTypes = items;
                this.docTypeSelected = this.docTypes[0];
            },
            error =>  {
                this.message = <any>error;
            }
        );
        this.service.getLabels('LABEL_ATRIBUTOS').subscribe(
            items => {
                const headerLabels: Dictionary<string> = {};
                for (let index = 0; index < items.length; index++) {
                    const key = items[index].id.toLowerCase();
                    headerLabels[key] = items[index].text;
                }
                this.titles = headerLabels;
            },
            error =>  {
                this.message = <any>error;
            }
        );
    }

    public find($ev, input: any) {
        this.isRetrieving = false;
        this.message = '';
        this.isQueried = false;

        $ev.preventDefault();
        // tslint:disable-next-line:forin
        for (const item in this.valForm.controls) {
            this.valForm.controls[item].markAsTouched();
        }
        if (input.searchDate) {
            this.isRetrieving = true;
            this.searchDate = input.searchDate;
            this.service.getMeeting(input.searchDate, this.docTypeSelected, input.documentId)
            .subscribe(
            items => {
                this.entries = items;
                const values = [];
                for (let index = 0; index < items.length; index++) {
                    const item = items[index];
                    let notes = '<table cellpadding="5" cellspacing="10">';
                    notes += this.createLine('Numero de Prescripción', item['noPrescripcion']);
                    notes += this.createLine('Fecha Prescripción', item['fPrescripcion']);
                    notes += this.createLine('Tipo de Tecnología', item['tipoTecnologia']);
                    notes += this.createLine('Cantidad de Tecnologías', item['consecutivo']);
                    notes += this.createLine('Paciente', `${item['tipoIDPaciente']}-${item['nroIDPaciente']}`);
                    notes += this.createLine('Observaciones', item['observaciones']);
                    notes += this.createLine('Cod Ent Proc', item['codEntProc']);
                    notes += this.createLine('Acta', item['noActa']);
                    notes += this.createLine('Fecha proceso', item['fProceso']);
                    notes += '</table>';

                    values.push({ info: notes });
                }
                this.items = values;
                this.isRetrieving = false;
                this.isQueried = true;
                this.onChangeTable(this.config);
            },
            error =>  {
                this.message = <any>error;
                this.isRetrieving = false;
            });
        }
    }

    private createLine(label: string, value: string): string {
        // tslint:disable-next-line:curly
        return (value) ? `<tr><td><b>${label}</b></td><td>&nbsp;</td><td>${value}</td></tr>` : '';
    }

    public selected(input: SelectItem): void {
        this.docTypeSelected = input.id;
    }

    public typed(input: string): void {
        this.docTypeSelected = input;
    }

    public exportToExcel(): void {
        const data = [];
        this.isRetrieving = true;
        for (let index = 0; index < this.entries.length; index++) {
            const item = this.entries[index];
            data.push({
                'NUMERO DE PRESCRIPCIÓN': item['noPrescripcion'],
                'FECHA PRESCRIPCIÓN': item['fPrescripcion'],
                'TIPO DE TECNOLOGÍA': item['tipoTecnologia'],
                'CANTIDAD DE TECNOLOGÍAS': item['consecutivo'],
                'PACIENTE': `${item['tipoIDPaciente']}-${item['nroIDPaciente']}`,
                'OBSERVACIONES': item['observaciones'] ? item['observaciones'] : '',
                'COD ENT PROC': item['codEntProc'] ? item['codEntProc'] : '',
                'ACTA': item['noActa'] ? item['noActa'] : '',
                'FECHA PROCESO': item['fProceso'] ? item['fProceso'] : ''
            });
        }
        const info: Dictionary<any[]> = {};
        info['junta'] = data;
        this.excelService.exportAsExcel(info, 'junta', 'Junta Medica');
        this.isRetrieving = false;
    }
}
