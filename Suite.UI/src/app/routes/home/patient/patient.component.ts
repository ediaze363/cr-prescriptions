import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { EventEmitter, Output, Input} from '@angular/core';

import { PrescriptionService } from '../services/prescription.service';
import { AuthService } from '../../../core/auth/auth.service';
import { Patient } from '../models/patient';
import { Column } from '../../../shared/directives/grid/column';
import { Dictionary } from '../models/dictionary';

@Component({
    selector: 'app-patient',
    templateUrl: './patient.component.html',
    styleUrls: ['./patient.component.scss']
})

export class PatientComponent implements OnInit {
    @Input()
    public labels: Dictionary<string>;

    public message: string;
    public docType: string;
    public docId: string;
    private user: any;
    private searchDate: string;
    public patient: Patient;
    public prescriptions: Array<any> = [];
    public medicines: Array<any> = [];
    public procedures: Array<any> = [];
    public devices: Array<any> = [];
    public nutritionalProducts: Array<any> = [];
    public complementaryServices: Array<any> = [];

    constructor(
        private service: PrescriptionService,
        private authService: AuthService,
        private route: ActivatedRoute,
        private router: Router,
        private history: Location
    ) {
    }
    public ngOnInit() {
    }

    @Input()
    set DataSource(selected: Patient) {
        if (selected) {
            this.patient = selected;
            this.prescriptions = this.getEntityData(selected['prescripcion']);
            this.medicines = this.getArrayData(selected['medicamentos'], 'principiosActivos');
            this.procedures = this.getArrayData(selected['procedimientos'], '');
            this.devices = this.getArrayData(selected['dispositivos'], '');
            this.nutritionalProducts = this.getArrayData(selected['productosnutricionales'], '');
            this.complementaryServices = this.getArrayData(selected['serviciosComplementarios'], '');
        }
    }

    private getEntityData(row: any): Array<any> {
        const lines: Array<any> = [];
        const items = this.getRowData(row, '');
        lines.push({ columns: items });
        return lines;
    }

    private getRowData(row: any, innerListName: string): Array<any> {
        const items: Array<any> = [];
        const tableList = innerListName.toLowerCase();
        for (const property in row) {
            if (row.hasOwnProperty(property)) {
                let label = property;
                const labelKey = property.toLowerCase();

                if (this.labels) {
                    label = this.labels[labelKey] ? this.labels[labelKey] : property;
                }

                let content = row[property];
                if (labelKey === tableList) {
                    const innerRows = this.getRowData(row[property], '');
                    let lines = '';
                    for (let index = 0; index < innerRows.length; index++) {
                        const entry = innerRows[index].value;
                        let line = '';
                        for (const attribute in entry) {
                            if (entry.hasOwnProperty(attribute)) {
                                if (line.length > 0) {
                                    line += ', ';
                                }
                                const text = entry[attribute].replace(',', '.');
                                let attrId = attribute;
                                const attrKey = attribute.toLowerCase();
                                if (this.labels) {
                                    attrId = this.labels[attrKey] ? this.labels[attrKey] : attrId;
                                }
                                line += `${attrId}: ${text}`;
                            }
                        }
                        if (lines.length > 0) {
                            lines += '; ';
                        }
                        lines += `${line}`;
                    }
                    content = lines;
                }

                items.push({ key: label, value: content });
            }
        }
        return items;
    }

    private getArrayData(source: Array<any>, innerListName: string): Array<any> {
        const lines: Array<any> = [];
        for (let index = 0; index < source.length; index++) {
            const row = source[index];
            const items = this.getRowData(row, innerListName);
            lines.push({ columns: items, index: index + 1 });
        }
        return lines;
    }
}
