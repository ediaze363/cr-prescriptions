import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Dictionary } from '../models/dictionary';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor() { }

  /* Original version */
  public exportAsExcelFile(data: any[], filename: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, filename);
  }

  /* New version. https://github.com/SheetJS/js-xlsx/issues/610 */
  public exportAsExcel(workSheetNames: Dictionary<any[]>, filename: string, title = 'Export', author = ''): void {
    const workbook: XLSX.WorkBook = { SheetNames: [], Sheets: {} };
    workbook.Props = {
      Title: title,
      Author: author
    };
    for (const workSheetName in workSheetNames) {
      if (workSheetNames.hasOwnProperty(workSheetName)) {
        const data = workSheetNames[workSheetName];
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
        XLSX.utils.book_append_sheet(workbook, worksheet, workSheetName);
      }
    }
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, filename);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + this.getTime() + EXCEL_EXTENSION);
  }

  private getTime(): string {
    const n = new Date();
    n.setDate(n.getDate() - 1);
    const y = n.getFullYear();
    const month: number = n.getMonth() + 1;
    const m = month < 10 ? '0' + month : month;
    const d = n.getDate() < 10 ? '0' + n.getDate() : n.getDate();
    return y + '' + m + '' + d;
  }
}
