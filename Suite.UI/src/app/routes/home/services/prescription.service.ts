import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { SettingsService } from '../../../core/settings/settings.service';
import { AuthService } from 'app/core/auth/auth.service';
import { Prescription } from '../models/prescription';
import { Patient } from '../models/Patient';
import { LabelText } from '../models/label.text';
import { ProfessionalMeeting } from '../models/professional.meeting';

@Injectable()
export class PrescriptionService {
    public token: string;
    private serviceUrl: string;

    public constructor(
        private http: Http,
        private settings: SettingsService,
        private authService: AuthService
    ) {
        // set token if saved in local storage
        const currentUser = this.authService.getCurrentUser();
        this.token = currentUser && currentUser.token;
        this.serviceUrl = this.settings.getAppSetting('apiUrl');
    }

    public getLabels(id: string): Observable<LabelText[]> {
        const params = new URLSearchParams();
        const options = new RequestOptions({
            headers: this.getHeaders(),
            search: params
        });
        const apiUrl = `${this.serviceUrl}api/SubGroups/all/${id}`;
        return this.http.get(apiUrl, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getInfo(date: string, type: string, document: string): Observable<Patient[]> {
        const params = new URLSearchParams();
        const options = new RequestOptions({
            headers: this.getHeaders(),
            search: params
        });
        let apiUrl = this.serviceUrl + 'api/patients/info/';
        if (document) {
            apiUrl = `${apiUrl}${date}/${type}/${document}`;
        } else {
            apiUrl = `${apiUrl}${date}/*/*`;
        }
        return this.http.get(apiUrl, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getMeeting(date: string, type: string, document: string): Observable<ProfessionalMeeting[]> {
        const params = new URLSearchParams();
        const options = new RequestOptions({
            headers: this.getHeaders(),
            search: params
        });
        let apiUrl = this.serviceUrl + 'api/patients/meeting/';
        if (document) {
            apiUrl = `${apiUrl}${date}/${type}/${document}`;
        } else {
            apiUrl = `${apiUrl}${date}/*/*`;
        }
        return this.http.get(apiUrl, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        const body = res.json();
        const result = body.data || { };
        if (body.data) {
            return result;
        }
        return body;
    }

    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            let err = '';
            try {
                const body = error.json() || '';
                err = body.error || JSON.stringify(body);
            } catch (e) {
                // No content response..
                const failure = <any>error;
                err = failure._body || 'Api Server Error';
            }
            if (error.status === 0) {
                errMsg = 'No se puede conectar con Prescripciones API. Por favor revise que esté corriendo el servicio.';
            }
            else {
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            }
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    private getHeaders(): Headers {
        let headers = new Headers();
        headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Content-Type', 'application/json');
        return headers;
    }
}
