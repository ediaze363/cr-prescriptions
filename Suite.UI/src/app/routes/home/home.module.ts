import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrescriptionService } from './services/prescription.service';

import { Ng2TableModule } from 'ng2-table/ng2-table';

import { SharedModule } from '../../shared/shared.module';
import { PatientComponent } from './patient/patient.component';

import { SelectModule } from 'ng2-select';
import { GridComponent } from '../../shared/directives/grid/grid.component';
import { MeetingComponent } from './meeting/meeting.component';
import { ExcelService } from './services/excel.service';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'patient/:id', component: PatientComponent },
    { path: 'meeting', component: MeetingComponent }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        Ng2TableModule,
        SelectModule
    ],
    providers: [
        PrescriptionService,
        ExcelService
    ],
    declarations: [
        HomeComponent,
        PatientComponent,
        MeetingComponent,
        GridComponent
    ],
    exports: [
        RouterModule,
        GridComponent
    ]
})
export class HomeModule { }
