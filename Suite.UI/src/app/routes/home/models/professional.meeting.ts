export class ProfessionalMeeting {
    public noPrescripcion?: string;
    public fPrescripcion?: string;
    public tipoTecnologia?: string;
    public consecutivo?: string;
    public estJM?: string;
    public codEntProc?: string;
    public observaciones?: string;
    public noActa?: string;
    public fProceso?: string;
    public tipoIDPaciente?: string;
    public nroIDPaciente?: string;
}
