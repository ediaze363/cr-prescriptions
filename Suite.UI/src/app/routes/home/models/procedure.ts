export class Procedure {
    public conOrden?: string;
    public tipoPrest?: string;
    public causaS11?: string;
    public causaS12?: string;
    public causaS2?: string;
    public causaS3?: string;
    public causaS4?: string;
    public proPBSUtilizado?: string;
    public causaS5?: string;
    public proPBSDescartado?: string;
    public rznCausaS51?: string;
    public descRzn51?: string;
    public rznCausaS52?: string;
    public descRzn52?: string;
    public causaS6?: string;
    public causaS7?: string;
    public codCUPS?: string;
    public canForm?: string;
    public codFreUso?: string;
    public cant?: string;
    public codPerDurTrat?: string;
    public justNoPBS?: string;
    public indRec?: string;
    public estJM?: string;
}
