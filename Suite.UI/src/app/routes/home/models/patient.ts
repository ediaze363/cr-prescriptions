import { Prescription } from './prescription';
import { Medicine } from './medicine';
import { Procedure } from './procedure';
import { NutritionalProduct } from './nutritional.product';
import { Device } from './device';
import { ComplementaryService } from './complementary.service';
export class Patient {
    public prescripcion?: Prescription;
    public medicamentos: Medicine[];
    public procedimientos: Procedure[];
    public dispositivos: Device[];
    public productosnutricionales: NutritionalProduct[];
    public serviciosComplementarios: ComplementaryService[];
}
