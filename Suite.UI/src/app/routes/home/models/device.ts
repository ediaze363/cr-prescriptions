export class Device {
    public conOrden?: string;
    public tipoPrest?: string;
    public causaS1?: string;
    public codDisp?: string;
    public canForm?: string;
    public codFreUso?: string;
    public cant?: string;
    public codPerDurTrat?: string;
    public justNoPBS?: string;
    public indRec?: string;
    public estJM?: string;
}
