export class ComplementaryService {
    public conOrden?: string;
    public tipoPrest?: string;
    public causaS1?: string;
    public causaS2?: string;
    public causaS3?: string;
    public causaS4?: string;
    public descCausaS4?: string;
    public causaS5?: string;
    public codSerComp?: string;
    public descSerComp?: string;
    public canForm?: string;
    public codFreUso?: string;
    public cant?: string;
    public codPerDurTrat?: string;
    public justNoPBS?: string;
    public indRec?: string;
    public estJM?: string;
}
