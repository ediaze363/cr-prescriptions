
const Home = {
    text: 'Prescripciones',
    link: '/home',
    icon: 'icon-home'
};

const Junta = {
    text: 'Junta Profesionales',
    link: '/home/meeting',
    icon: 'icon-people'
};

const Master = {
    text: 'Maestros',
    link: '/master',
    icon: 'icon-settings',
    submenu: [
        {
            text: 'Grupos',
            link: '/master/group'
        },
        {
            text: 'Subgrupos',
            link: '/master/subgroup'
        }
    ]
};

const headingMain = {
    text: 'MENU',
    heading: true
};

export const menu = [
    headingMain,
    Home,
    Junta
];
