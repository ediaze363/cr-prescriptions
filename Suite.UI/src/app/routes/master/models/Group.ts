export class Group {
    public id?: string;
    public code?: string;
    public name?: string;
}
