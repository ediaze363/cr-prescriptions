import { Component, OnInit } from '@angular/core';
import { NavsearchService } from '../../../layout/header/navsearch/navsearch.service';


@Component({
    selector: 'app-subgroup',
    templateUrl: './subgroup.component.html',
    styleUrls: ['./subgroup.component.scss']
})
export class SubgroupComponent implements OnInit {
    private subscription: any;
    public message: string;

    constructor(
        private navService: NavsearchService
    ) { }

    ngOnInit() {
        this.subscription = this.navService.getNavChangeEmitter()
        .subscribe(term => this.findTerm(term));
    }

    findTerm(term: string) {
        console.log('SubGroup search: ' + term);
        this.message = 'Buscando ' + term + '...';
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
