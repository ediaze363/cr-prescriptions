import { NgModule } from '@angular/core';
import { GroupComponent } from './group/group.component';
import { SubgroupComponent } from './subgroup/subgroup.component';
import { Routes, RouterModule } from '@angular/router';
import { DataTableModule } from 'angular2-datatable';

const routes: Routes = [
    { path: '', redirectTo: 'master' },
    { path: 'group', component: GroupComponent },
    { path: 'subgroup', component: SubgroupComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        DataTableModule
    ],
    declarations: [
        GroupComponent,
        SubgroupComponent
    ],
    exports: [
        RouterModule
    ]
})
export class MasterModule { }
