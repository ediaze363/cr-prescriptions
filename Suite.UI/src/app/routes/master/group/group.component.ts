import { Component, OnInit } from '@angular/core';
import { NavsearchService } from '../../../layout/header/navsearch/navsearch.service';


@Component({
    selector: 'app-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {
    private subscription: any;
    public message: string;

    constructor(
        private navService: NavsearchService
    ) { }

    ngOnInit() {
        this.subscription = this.navService.getNavChangeEmitter()
        .subscribe(term => this.findTerm(term));
    }

    findTerm(term: string) {
        console.log('Group search: ' + term);
        this.message = 'Buscando ' + term + '...';
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
