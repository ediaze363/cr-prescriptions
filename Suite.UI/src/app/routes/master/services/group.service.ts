import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { SettingsService } from '../../../core/settings/settings.service';
import { AuthService } from 'app/core/auth/auth.service';
import { Group } from '../models/Group';

@Injectable()
export class GroupService {
    public token: string;
    private baseUrl: string;

    public constructor(
        private router: Router,
        private http: Http,
        private settings: SettingsService,
        private authService: AuthService
    ) {
            // set token if saved in local storage
            const currentUser = this.authService.getCurrentUser();
            this.token = currentUser && currentUser.token;
            this.baseUrl = this.settings.getAppSetting('apiUrl') + 'Groups/';
    }

    public getAll(term: string): Observable<Group[]> {
        const params = new URLSearchParams();
        params.set('searchCondition', term);
        const options = new RequestOptions({
            headers: this.getHeaders(),
            search: params
        });

        return this.http.get(this.baseUrl + 'all/', options)
            .map((res: Response) => {
                const items: Group[] = <Group[]>res.json();
                return items;
        });
    }

    private getHeaders(): Headers {
        let headers = new Headers();
        headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Content-Type', 'application/json');
        return headers;
    }
}
