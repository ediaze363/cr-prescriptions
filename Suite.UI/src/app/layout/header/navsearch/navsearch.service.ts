import {EventEmitter} from '@angular/core';

export class NavsearchService {
  navchange: EventEmitter<string> = new EventEmitter();
  constructor() {}
  public emitNavChangeEvent(string) {
    this.navchange.emit(string);
  }
  getNavChangeEmitter() {
    return this.navchange;
  }
}
