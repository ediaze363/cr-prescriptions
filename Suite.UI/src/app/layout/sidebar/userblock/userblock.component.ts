import { Component, OnInit } from '@angular/core';

import { UserblockService } from './userblock.service';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
    constructor(
        public userblockService: UserblockService,
        private authService: AuthService
    ) {
        const context = this.authService.getCurrentUser();
        const picture = context.user.picture ? context.user.picture : 'user.png';

        this.user = {
            name: context.user.fullName,
            job: context.user.role,
            picture: 'assets/img/user/' + picture
        };
    }

    ngOnInit() {
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}
