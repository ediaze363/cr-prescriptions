﻿using Suite.Domain.Interfaces;
using System.Collections.Generic;
using Suite.Domain.Entities;
using Suite.Persistence.Resources;
using Microsoft.Extensions.Options;
using Suite.Domain.Dtos;

namespace Suite.Persistence
{
    public class GroupRepository : BaseRepository, IGroupRepository
    {
        /// <summary>
        /// Repository initialization
        /// </summary>
        /// <param name="config">Application settings</param>
        public GroupRepository(IOptions<AppSettings> config) : base(config)
        {
        }

        public IList<Group> GetAll(string term)
        {
            string sql = Queries.GetAllGroups;
            sql = sql.Replace("@term", term.Replace("'", "''"));
            var groups = GetAll<Group>(sql);
            return groups;
        }
    }
}
