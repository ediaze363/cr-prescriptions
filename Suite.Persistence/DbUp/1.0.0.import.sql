USE BASDAT
GO
TRUNCATE TABLE [dbo].[ste_Import]
GO
SELECT CODE, NAME, NOTES, ROW_NUMBER() OVER(PARTITION BY CODE ORDER BY LEN(name+isnull(notes,'')) DESC) AS RowNum FROM ste_Import ORDER BY code
GO
-- SELECT * INTO ste_ImportCopy FROM ste_Import
GO
INSERT INTO ste_Import
SELECT CODE, NAME, NOTES
FROM (
SELECT CODE, NAME, NOTES, ROW_NUMBER() OVER(PARTITION BY CODE ORDER BY LEN(NAME+ISNULL(NOTES,'')) DESC) AS ROWNUM
FROM STE_IMPORTCOPY
) COPY
WHERE ROWNUM = 1
GO
INSERT INTO ste_SubGroups (groupId, code, [name], notes)
SELECT '1967f38b-228b-e711-b6ea-00090ffe0001', [code], [name], notes
FROM ste_import
GO
SELECT g.code, g.[name], count(s.id) items
FROM ste_groups g left join ste_SubGroups s on s.GroupId = g.id 
GROUP BY g.code, g.[name], g.id
ORDER BY g.code
GO
SELECT * FROM STE_SUBGROUPS WHERE CODE = '1'
GO
SELECT g.code, s.code, s.[name], g.id
FROM ste_groups g inner join ste_SubGroups s on s.GroupId = g.id 
WHERE g.code = '00.LABEL_ATRIBUTOS' --s.code = 'DurTrat'
ORDER BY s.[name]
GO
SELECT g.code as groupCode, s.code, s.[name]
FROM ste_groups g inner join ste_SubGroups s on s.GroupId = g.id
ORDER BY g.code, s.[name]
GO
SELECT s.code, s.[name]
FROM ste_groups g inner join ste_SubGroups s on s.GroupId = g.id 
WHERE g.code = '01.TIPO_DOCUMENTO_IDEN'
ORDER BY s.[name]
GO
select * from ste_groups where id = 'a5feab50-158b-e711-b6ea-00090ffe0001'
go
select * from ste_import where code = 'U069'
go
update ste_Import
set [name] = 'Enfermedad por virus Zika, no especificado [Fiebre por virus Zika; Infecci�n por el virus Zika; Encefalitis Zika]'
where code = 'U069'
go
delete ste_subgroups where groupid = '56c6b6d5-af8a-e711-b6e7-00090ffe0001'