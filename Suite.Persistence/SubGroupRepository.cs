﻿using Microsoft.Extensions.Options;
using Suite.Domain.Dtos;
using Suite.Domain.Entities;
using Suite.Domain.Interfaces;
using Suite.Persistence.Resources;
using System.Collections.Generic;

namespace Suite.Persistence
{
    public class SubGroupRepository : BaseRepository, ISubGroupRepository
    {
        /// <summary>
        /// Repository initialization
        /// </summary>
        /// <param name="config">Application settings</param>
        public SubGroupRepository(IOptions<AppSettings> config) : base(config)
        {
        }

        public IList<SubGroup> GetAll(string groupCode)
        {
            groupCode = groupCode.Replace("'", "''");
            var items = GetAll<SubGroup>(Queries.GetAllSubGroups, new { groupCode });
            return items;
        }

        public IList<SubGroup> GetAll()
        {
            var items = GetAll<SubGroup>(Queries.GetAllGroupsSubgroups);
            return items;
        }
    }
}
