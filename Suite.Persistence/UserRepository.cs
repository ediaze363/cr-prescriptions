﻿using Suite.Domain.Dtos;
using Suite.Domain.Interfaces;
using Microsoft.Extensions.Options;
using Suite.Persistence.Resources;

namespace Suite.Persistence
{
    /// <summary>
    /// Access the database for the user entity
    /// </summary>
    public class UserRepository: BaseRepository, IUserRepository
    {
        /// <summary>
        /// Repository initialization
        /// </summary>
        /// <param name="config">Application settings</param>
        public UserRepository(IOptions<AppSettings> config) : base(config)
        {
        }

        /// <summary>
        /// Validate 
        /// </summary>
        /// <param name="organizationId">Organization</param>
        /// <param name="username">Username or email</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        public LogonUser Login(string organizationId, string username, string password)
        {
            var user = Get<LogonUser>(Queries.ValidateLogin, 
                new {
                    username,
                    password,
                    organizationId
                });

            return user;
        }
    }
}
