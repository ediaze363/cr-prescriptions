﻿using Dapper;
using Microsoft.Extensions.Options;
using Suite.Domain.Dtos;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Suite.Persistence
{
    /// <summary>
    /// Base class to access database
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// Main connection string to the database
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Initial instance configuration
        /// </summary>
        /// <param name="config">Application settings</param>
        public BaseRepository(
            IOptions<AppSettings> config
        )
        {
            _connectionString = config.Value.ConnectionString;
        }

        /// <summary>
        /// Get the first or default element from de query result
        /// </summary>
        /// <typeparam name="TEntity">Entity</typeparam>
        /// <param name="sql">Query statement</param>
        /// <param name="param">Filter parameters</param>
        /// <returns>Single entity</returns>
        public TEntity Get<TEntity>(string sql, object param = null)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<TEntity>(sql, param).FirstOrDefault();
                return result;
            }
        }

        /// <summary>
        /// Execute a statement to the database
        /// </summary>
        /// <param name="sql">Query statement</param>
        /// <param name="param">Filter parameters</param>
        /// <returns>Rows affected</returns>
        public int Execute(string sql, object param = null)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var rows = db.Execute(sql, param);
                return rows;
            }
        }

        /// <summary>
        /// Retrieve the entities that match the records
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="sql">Query statement</param>
        /// <param name="param">Filter parameters</param>
        /// <returns>Entity collection</returns>
        public IList<TEntity> GetAll<TEntity>(string sql, object param = null)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var result = db.Query<TEntity>(sql, param).ToList();
                return result;
            }
        }
    }
}
