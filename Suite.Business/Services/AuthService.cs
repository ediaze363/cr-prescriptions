﻿using Suite.Domain.Interfaces;
using Suite.Domain.Dtos;
using Microsoft.Extensions.Options;

namespace Suite.Business.Services
{
    public class AuthService: IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICryptoService _cryptoService;
        private readonly bool encryptPassword; 

        public AuthService(
            IUserRepository userRepository, 
            ICryptoService cryptoService,
            IOptions<AppSettings> config
        )
        {
            _userRepository = userRepository;
            _cryptoService = cryptoService;
            encryptPassword = config.Value.EncryptPassword;
        }

        public LogonUser Login(string organization, string username, string password)
        {
            if (encryptPassword)
            {
                password = _cryptoService.Encrypt(password);
            }
            return _userRepository.Login(organization, username, password);
        }
    }
}
