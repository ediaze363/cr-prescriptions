﻿using Suite.Domain.Dtos;
using Suite.Domain.Entities;
using Suite.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;

namespace Suite.Business.Services
{
    public class TranslateService
    {
        protected ISubGroupRepository _subgroupsRepository;
        protected IList<SubGroup> _subgroups;

        protected bool GetDate(string date)
        {
            DateTime searchDate = DateTime.Now;
            var culture = System.Globalization.CultureInfo.InvariantCulture;
            var isValid = DateTime.TryParse(date, culture, System.Globalization.DateTimeStyles.AssumeLocal, out searchDate);
            return isValid;
        }

        protected void FireException(Exception ex)
        {
            ex = ex.GetBaseException();
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            throw new HttpRequestException("Error accediendo al servicio de MiPress", ex);
        }        

        /// <summary>
        /// Convert the codes the corresponding names and descriptions
        /// </summary>
        /// <param name="patients">Patient collection</param>
        /// <returns></returns>
        protected List<ProfessionalMeeting> TranslateCodes(List<ProfessionalMeeting> items)
        {
            if (_subgroups == null)
            {
                _subgroups = _subgroupsRepository.GetAll();
                if (_subgroups == null)
                {
                    return items;
                }
            }

            var translate = GetTranslateDictionary();
            if (translate == null)
            {
                return items;
            }

            items.ForEach(m => AssignValues(translate, m, m.GetType().GetProperties().ToList()));

            return items;
        }


        /// <summary>
        /// Convert the codes the corresponding names and descriptions
        /// </summary>
        /// <param name="items">Patient collection</param>
        /// <returns></returns>
        protected List<PatientModel> TranslateCodes(List<PatientModel> items)
        {
            if (_subgroups == null)
            {
                _subgroups = _subgroupsRepository.GetAll();
                if (_subgroups == null)
                {
                    return items;
                }
            }

            var translate = GetTranslateDictionary();
            if (translate == null)
            {
                return items;
            }

            items.ForEach(line =>
            {
                AssignValues(translate, line.prescripcion, line.prescripcion.GetType().GetProperties().ToList());
                line.medicamentos.ForEach(m => {
                    AssignValues(translate, m, m.GetType().GetProperties().ToList());
                    m.PrincipiosActivos.ForEach(p => AssignValues(translate, p, p.GetType().GetProperties().ToList()));
                });
                line.procedimientos.ForEach(m => AssignValues(translate, m, m.GetType().GetProperties().ToList()));
                line.dispositivos.ForEach(m => AssignValues(translate, m, m.GetType().GetProperties().ToList()));
                line.productosnutricionales.ForEach(m => AssignValues(translate, m, m.GetType().GetProperties().ToList()));
                line.serviciosComplementarios.ForEach(m => AssignValues(translate, m, m.GetType().GetProperties().ToList()));
            });

            return items;
        }

        protected void AssignValues(Dictionary<string, string> translate, BaseDto entity, List<PropertyInfo> properties)
        {
            properties.ForEach(propertyInfo =>
            {
                var code = propertyInfo.Name.ToLower();
                var master = string.Empty;
                if (translate.TryGetValue(code, out master))
                {
                    var value = propertyInfo.GetValue(entity);
                    if (value != null)
                    {
                        var filter = value.ToString();
                        var name = GetName(master, filter);
                        propertyInfo.SetValue(entity, name);
                    }
                }
            });
        }

        protected Dictionary<string, string> GetTranslateDictionary()
        {
            return _subgroups
                            .Where(g => g.Master != null)
                            .Select(e => new { e.Code, e.Master })
                            .GroupBy(e => e.Code)
                            .Select(g => g.FirstOrDefault())
                            .ToDictionary(k => k.Code.ToLower(), v => v.Master);
        }

        /// <summary>
        /// Get the name of the description code
        /// </summary>
        /// <param name="groupCode">Group Code</param>
        /// <param name="subgroupCode">Subgroup code</param>
        /// <param name="groupSubgroups">Group collections</param>
        /// <returns></returns>
        protected string GetName(string groupCode, string subgroupCode)
        {
            subgroupCode = subgroupCode ?? String.Empty;
            var subgroup = _subgroups.Where(s => s.GroupCode == groupCode && s.Code == subgroupCode).SingleOrDefault();
            return subgroup?.Name ?? subgroupCode;
        }
    }
}
