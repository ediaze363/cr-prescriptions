﻿using Newtonsoft.Json;
using Suite.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace Suite.Business.Services
{
    using Domain.Dtos;
    using Domain.Entities;
    using RestSharp;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// Service access to MinSalud Source
    /// </summary>
    public class PatientService: TranslateService, IPatientService
    {
        public PatientService(ISubGroupRepository subgroupsRepository)
        {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<PrescriptionDto, Prescription>();
            });
            _subgroupsRepository = subgroupsRepository; 
        }

        /// <summary>
        /// Retrieve all information from MinSalud web service
        /// <![CDATA[
        /// AutoMapper Tutorial
        /// https://dotnetcademy.net/Learn/2/Pages/1
        /// ]]>
        /// </summary>
        /// <param name="endpoint">EndPoint URL</param>
        /// <param name="date">Query Date</param>
        /// <param name="type">Document Type</param>
        /// <param name="document">Document Number</param>
        /// <returns>Prescription list</returns>
        public List<PatientModel> GetPatients(SourceEndPoint endpoint, string date, string type, string document)
        {
            bool isValid = GetDate(date);
            if (!isValid)
            {
                return new List<PatientModel>();
            }

            var nit = endpoint.Nit;
            var token = endpoint.Token;
            var apiUrl = !endpoint.Url.EndsWith("/") ? $"{endpoint.Url}/" : endpoint.Url;

            apiUrl = string.IsNullOrEmpty(document) ?
                $"{apiUrl}Prescripcion/{nit}/{date}/{token}" :
                $"{apiUrl}PrescripcionPaciente/{nit}/{date}/{token}/{type}/{document}";

            var json = string.Empty;
            try
            {
                json = GetDataAsync(apiUrl).Result;
            }
            catch (Exception ex)
            {
                FireException(ex);
            }

            var items = JsonConvert.DeserializeObject<List<PatientModel>>(json);

            items.ForEach(p =>
            {
                p.prescripcion.HPrescripcion = p.prescripcion.HPrescripcion.Substring(0, 8);
                p.prescripcion.FPrescripcion = p.prescripcion.FPrescripcion.Substring(0, 10);
            });

            items = TranslateCodes(items);

            var entries = items.ToList()
                .OrderBy(p => p.prescripcion.PAPaciente)
                .ThenBy(p => p.prescripcion.PNPaciente)
                .ThenBy(p => p.prescripcion.HPrescripcion)
                .ToList();

            return entries;
        }
        /// <summary>
        /// Retorna el estado de la Junta de Profesionales para la prescripción que se ingresó
        /// </summary>
        /// <param name="endpoint">Endpoint de mipress</param>
        /// <param name="date">fecha de consulta</param>
        /// <param name="type">Tipo de documento del paciente</param>
        /// <param name="document">Número de documento del paciente</param>
        /// <param name="prescriptionId">Número de prescripción</param>
        /// <returns>Lista de profesionales</returns>
        public List<ProfessionalMeeting> GetProfessionalMeeting(SourceEndPoint endpoint, string date, string type, string document, string prescriptionId)
        {
            bool isValid = GetDate(date);
            if (!isValid)
            {
                return new List<ProfessionalMeeting>();
            }

            var nit = endpoint.Nit;
            var token = endpoint.Token;
            var apiUrl = !endpoint.Url.EndsWith("/") ? $"{endpoint.Url}/" : endpoint.Url;

            apiUrl = !string.IsNullOrEmpty(prescriptionId) ?
                $"{apiUrl}JuntaProfesional/{nit}/{token}/{prescriptionId}" :
                string.IsNullOrEmpty(document) ?
                $"{apiUrl}JuntaProfesionalXFecha/{nit}/{token}/{date}":
                $"{apiUrl}JuntaProfesionalXPaciente/{nit}/{date}/{token}/{type}/{document}";

            var json = string.Empty;
            try
            {
                json = GetDataAsync(apiUrl).Result;
            }
            catch (Exception ex)
            {
                FireException(ex);
            }

            var data = JsonConvert.DeserializeObject<List<ProfessionalMeetingModel>>(json);
            var items = new List<ProfessionalMeeting>();
            if (data.Count > 0)
            {
                items = data.Select(m => m.Prescription).ToList();
            }

            items = TranslateCodes(items);

            items.ForEach(p =>
            {
                p.FPrescripcion = p.FPrescripcion.Substring(0, 10);
            });

            var entries = items.ToList()
                .OrderBy(p => p.NoPrescripcion)
                .ToList();

            return entries;
        }

        /// <summary>
        /// Get the data
        /// </summary>
        /// <example>
        /// RestSharp   http://restsharp.org/
        /// </example>
        /// <param name="apiUrl"></param>
        /// <param name="useHttpApi"></param>
        /// <returns></returns>
        protected async Task<string> GetDataAsync(string apiUrl, bool useHttpApi = true)
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            var result = string.Empty;

            stopwatch.Start();

            if (useHttpApi)
            {
                var restClient = new RestClient(apiUrl);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Accept", "application/json");

                TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

                Console.WriteLine(restClient.Timeout); 
                RestRequestAsyncHandle handle = restClient.ExecuteAsync(
                    request, r => taskCompletion.SetResult(r));

                RestResponse response = (RestResponse)(await taskCompletion.Task);
                result = response.Content;
            }
            else
            {
                // apiUrl = "http://localhost:4200/assets/server/2017-08-24.json";
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    var response = client.GetStringAsync(apiUrl);
                    result = response.Result;
                }
            }

            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

            return result;
        }
    }
}
