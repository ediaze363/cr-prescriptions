﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Suite.Domain.Dtos;
using Suite.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Suite.RestApi.Controllers
{
    /// <summary>
    /// Interface de comunicación con el servicio de externo https://wsmipres.sispro.gov.co/wsmipresnopbs/api
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class SubGroupsController : Controller
    {
        private readonly ILogger _logger;
        private readonly AppSettings _settings;
        private readonly ISubGroupRepository _database;

        public SubGroupsController(
            ILogger<PatientsController> logger,
            IOptions<AppSettings> config,
            ISubGroupRepository database)
        {
            _logger = logger;
            _settings = config.Value;
            _database = database;
        }

        /// <summary>
        /// Retorna el listado de los pacientes y sus prescripciones para una fecha
        /// </summary>
        /// <param name="date">Fecha de consulta en formato AAAA-MM-DD</param>
        /// <returns></returns>
        // GET api/SubGroups/all/{id}
        [HttpGet("{id}")]
        [ActionName("all")]
        public IActionResult GetByGroup(string id)
        {
            IList<LabelDto> items = null;

            try
            {
                var subGroups = _database.GetAll(id);
                items = subGroups.Select(g => new LabelDto { Id = g.Code, Text = g.Name }).ToList();
            }
            catch (Exception ex)
            {
                return LogError(ControllerContext.RouteData.Values, ex);
            }
            return new ObjectResult(items);
        }

        /// <summary>
        /// Error handling
        /// </summary>
        /// <param name="values"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private IActionResult LogError(Microsoft.AspNetCore.Routing.RouteValueDictionary values, Exception ex)
        {
            ex = ex.GetBaseException();
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            string error = ex.Message;
            return StatusCode(500, error);
        }
    }
}
