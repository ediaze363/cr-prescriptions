﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Suite.Domain.Dtos;
using Suite.Domain.Interfaces;
using Suite.RestApi.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Suite.RestApi.Controllers
{
    /// <summary>
    /// # Front-End
    /// http://jasonwatmore.com/post/2016/08/16/angular-2-jwt-authentication-example-tutorial
    /// 
    /// # Back-End
    /// https://stormpath.com/blog/token-authentication-asp-net-core
    /// https://github.com/nbarbettini/SimpleTokenProvider
    /// </summary>
    public class TokenProviderMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;

        public TokenProviderMiddleware(
            RequestDelegate next,
            IOptions<TokenProviderOptions> options,
            IServiceProvider serviceProvider,
            IOptions<AppSettings> config)
        {
            var authService = serviceProvider.GetService<IAuthService>();
            _next = next;
            _options = options.Value;
            _options.IdentityResolver = authService.Login;

            ThrowIfInvalidOptions(_options);
        }

        public static TokenProviderOptions CreateTokenProviderOptions()
        {
            // Add JWT generation endpoint:
            var signingKey = new SymmetricSecurityKey(System.Text.Encoding.ASCII.GetBytes(AppSettings.SecretKey));
            var options = new TokenProviderOptions
            {
                Audience = "Audience",
                Issuer = "Issuer",
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
            };

            return options;
        }

        public Task Invoke(HttpContext context)
        {
            Task result = null;

            try
            {
                // If the request path doesn't match, skip
                if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
                {
                    return _next(context);
                }

                // Request must be POST with Content-Type: application/x-www-form-urlencoded
                if (!context.Request.Method.Equals("POST") || !context.Request.HasFormContentType)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return context.Response.WriteAsync("Solicitud incorrecta.");
                }

                result = GenerateToken(context);
                if (result.Exception != null)
                {
                    throw result.Exception;
                }
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return context.Response.WriteAsync(ex.GetBaseException().Message);
            }

            return result;
        }

        /// <summary>
        /// Get the token for the request
        /// </summary>
        /// <example>https://en.wikipedia.org/wiki/List_of_HTTP_status_codes</example>
        /// <param name="context"></param>
        /// <returns></returns>
        private async Task GenerateToken(HttpContext context)
        {
            var username = context.Request.Form["username"];
            var password = context.Request.Form["password"];
            var organization = context.Request.Form["organization"];

            var user = _options.IdentityResolver(organization, username, password);
            var identity = await GetIdentity(user);
            if (identity == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                await context.Response.WriteAsync("Usuario o contraseña incorrecto.");
                return;
            }

            string encodedJwt = await GetClaimsToken(username);
            var authSettings = new
            {
                access_token = encodedJwt,
                expires_in = (int)_options.Expiration.TotalSeconds,
                user = new
                {
                    username = user.Username,
                    id = user.UserId,
                    fullName = user.FullName,
                    role = user.Role,
                    picture = user.Picture,
                    organization = user.Organization
                },
                service = new
                {
                    username = "",
                    url = "",
                    token = ""
                }
            };

            // Serialize and return the response
            context.Response.ContentType = "application/json";
            var logonUser = JsonConvert.SerializeObject(authSettings, new JsonSerializerSettings { Formatting = Formatting.Indented });
            await context.Response.WriteAsync(logonUser);
        }

        private async Task<string> GetClaimsToken(string username)
        {
            var now = DateTime.UtcNow;

            // Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.
            // You can add other claims here, if you want:
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, await _options.NonceGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64)
            };

            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(_options.Expiration),
                signingCredentials: _options.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        private Task<ClaimsIdentity> GetIdentity(LogonUser logonUser)
        {
            if (logonUser != null && logonUser.UserId != Guid.Empty)
            {
                return Task.FromResult(new ClaimsIdentity(
                    new System.Security.Principal.GenericIdentity(logonUser.Username, "Token"),
                    new Claim[] { }));
            }
            // Credentials are invalid, or account doesn't exist
            return Task.FromResult<ClaimsIdentity>(null);
        }

        private static void ThrowIfInvalidOptions(TokenProviderOptions options)
        {
            if (string.IsNullOrEmpty(options.Path))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Path));
            }

            if (string.IsNullOrEmpty(options.Issuer))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Issuer));
            }

            if (string.IsNullOrEmpty(options.Audience))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Audience));
            }

            if (options.Expiration == TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenProviderOptions.Expiration));
            }

            if (options.IdentityResolver == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.IdentityResolver));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.SigningCredentials));
            }

            if (options.NonceGenerator == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.NonceGenerator));
            }
        }

        /// <summary>
        /// Get this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        public static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}
