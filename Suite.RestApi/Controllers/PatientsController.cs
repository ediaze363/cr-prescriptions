﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Suite.Domain.Dtos;
using Suite.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace Suite.RestApi.Controllers
{
    /// <summary>
    /// Interface de comunicación con el servicio de externo https://wsmipres.sispro.gov.co/wsmipresnopbs/api
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class PatientsController : Controller
    {
        private readonly ILogger _logger;
        private readonly AppSettings _settings;
        private readonly IPatientService _patientService;

        public PatientsController(
            ILogger<PatientsController> logger,
            IOptions<AppSettings> config,
            IPatientService patientService)
        {
            _logger = logger;
            _settings = config.Value;
            _patientService = patientService;
        }

        // GET api/patients/test/{id}
        [HttpGet("{id}")]
        [ActionName("test")]
        public IActionResult GetTest()
        {
            var info = new { MachineName = Environment.MachineName, Date = DateTime.Now, Version = _settings.Version };
            return new ObjectResult(info);
        }

        /// <summary>
        /// Retorna la lista de prescripciones, con la lista de los medicamentos asociados a la prescripción, con la lista 
        /// de procedimientos asociados a la prescripción, la lista de dispositivos médicos asociados a la prescripción, con 
        /// la lista de los productos nutricionales asociados a la prescripción y con la lista de los servicios complementarios 
        /// asociados a la prescripción.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="type"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        // GET api/patients/info/{date}/{type}/{doc}
        [HttpGet("{date}/{type}/{doc}")]
        [ActionName("info")]
        public IActionResult GetPatients(string date, string type, string doc)
        {
            List<PatientModel> items = null;
            try
            {
                items = GetPatientsInfo(date, type, doc);
            }
            catch (Exception ex)
            {
                return LogError(ControllerContext.RouteData.Values, ex);
            }
            return new ObjectResult(items);
        }

        /// <summary>
        /// Retorna la lista de prescripciones, con la lista de los medicamentos asociados a la prescripción, con la lista 
        /// de procedimientos asociados a la prescripción, la lista de dispositivos médicos asociados a la prescripción, con 
        /// la lista de los productos nutricionales asociados a la prescripción y con la lista de los servicios complementarios 
        /// asociados a la prescripción.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="type"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        // GET api/patients/meeting/{date}/{type}/{doc}
        [HttpGet("{date}/{type}/{doc}")]
        [ActionName("meeting")]
        public IActionResult GetProfessionalMeeting(string date, string type, string doc)
        {
            List<ProfessionalMeeting> items = null;
            try
            {
                items = GetProfessionalMeetingInfo(date, type, doc);
            }
            catch (Exception ex)
            {
                return LogError(ControllerContext.RouteData.Values, ex);
            }
            return new ObjectResult(items);
        }

        /// <summary>
        /// Get all patient information
        /// </summary>
        /// <param name="date"></param>
        /// <param name="type"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        private List<PatientModel> GetPatientsInfo(string date, string type, string doc)
        {
            type = type == "*" ? null : type;
            doc = doc == "*" ? null : doc;
            var items = _patientService.GetPatients(_settings.SourceEndPoint, date, type, doc);
            return items;
        }

        // <summary>
        /// Get all patient information
        /// </summary>
        /// <param name="date"></param>
        /// <param name="type"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        private List<ProfessionalMeeting> GetProfessionalMeetingInfo(string date, string type, string doc)
        {
            type = type == "*" ? null : type;
            doc = doc == "*" ? null : doc;
            var items = _patientService.GetProfessionalMeeting(_settings.SourceEndPoint, date, type, doc, null);
            return items;
        }

        /// <summary>
        /// Error handling
        /// </summary>
        /// <param name="values"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private IActionResult LogError(Microsoft.AspNetCore.Routing.RouteValueDictionary values, Exception ex)
        {
            ex = ex.GetBaseException();
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            string error = ex.Message;
            return StatusCode(500, error);
        }
    }
}
