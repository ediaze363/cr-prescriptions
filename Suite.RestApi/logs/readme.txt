﻿# ASP.NET Web API Help Pages using Swagger
https://docs.microsoft.com/en-us/aspnet/core/tutorials/web-api-help-pages-using-swagger?tabs=visual-studio
----------------------------------------------------------------------------------------------------------
PM> Install-Package Swashbuckle.AspNetCore
----------------------------------------------------------------------------------------------------------

# ASP.NET Core Token Authentication Guide
https://stormpath.com/blog/token-authentication-asp-net-core
----------------------------------------------------------------------------------------------------------
PM> Install-Package System.IdentityModel.Tokens.Jwt
----------------------------------------------------------------------------------------------------------

# Newtonsoft.Json
https://www.newtonsoft.com/json/help/html/SerializingJSON.htm
----------------------------------------------------------------------------------------------------------
PM> Install-Package Newtonsoft.Json
----------------------------------------------------------------------------------------------------------
