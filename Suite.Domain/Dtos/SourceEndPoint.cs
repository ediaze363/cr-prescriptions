﻿namespace Suite.Domain.Dtos
{
    /// <summary>
    /// Attributes to access the external EndPoint
    /// </summary>
    /// <example>
    /// <![CDATA[
    /// https://wsmipres.sispro.gov.co/wsmipresnopbs/Swagger/ui/index#!/Prescripcion/GetPrescripcion
    /// ]]>
    /// </example>
    public class SourceEndPoint
    {
        /// <summary>
        /// URL to access the WS
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// Username for service access
        /// </summary>
        public string Nit { get; set; }
        /// <summary>
        /// Password for services access
        /// </summary>
        public string Token { get; set; }
    }
}
