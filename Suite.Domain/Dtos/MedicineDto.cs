﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Suite.Domain.Dtos
{
    public class MedicineDto: BaseDto
    {
        public string ConOrden { get; set; }
        public string TipoMed { get; set; }
        public string TipoPrest { get; set; }
        public string CausaS1 { get; set; }
        public string CausaS2 { get; set; }
        public string CausaS3 { get; set; }
        public string MedPBSUtilizado { get; set; }
        public string RznCausaS31 { get; set; }
        public string DescRzn31 { get; set; }
        public string RznCausaS32 { get; set; }
        public string DescRzn32 { get; set; }
        public string CausaS4 { get; set; }
        public string MedPBSDescartado { get; set; }
        public string RznCausaS41 { get; set; }
        public string DescRzn41 { get; set; }
        public string RznCausaS42 { get; set; }
        public string DescRzn42 { get; set; }
        public string RznCausaS43 { get; set; }
        public string DescRzn43 { get; set; }
        public string RznCausaS44 { get; set; }
        public string DescRzn44 { get; set; }
        public string CausaS5 { get; set; }
        public string RznCausaS5 { get; set; }
        public string CausaS6 { get; set; }
        public string DescMedPrinAct { get; set; }
        public string CodFF { get; set; }
        public string CodVA { get; set; }
        public string JustNoPBS { get; set; }
        public string Dosis { get; set; }
        public string DosisUM { get; set; }
        public string NoFAdmon { get; set; }
        public string CodFreAdmon { get; set; }
        public string IndEsp { get; set; }
        public string CanTrat { get; set; }
        public string DurTrat { get; set; }
        public string CantTotalF { get; set; }
        public string UFCantTotal { get; set; }
        public string IndRec { get; set; }
        public string EstJM { get; set; }
        [JsonProperty("principiosActivos")]
        public List<PrincipioActivo> PrincipiosActivos { get; set; }
    }
}
