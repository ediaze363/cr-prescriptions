﻿namespace Suite.Domain.Dtos
{
    public class ComplementaryServiceDto: BaseDto
    {
        public string ConOrden { get; set; }
        public string TipoPrest { get; set; }
        public string CausaS1 { get; set; }
        public string CausaS2 { get; set; }
        public string CausaS3 { get; set; }
        public string CausaS4 { get; set; }
        public string DescCausaS4 { get; set; }
        public string CausaS5 { get; set; }
        public string CodSerComp { get; set; }
        public string DescSerComp { get; set; }
        public string CanForm { get; set; }
        public string CodFreUso { get; set; }
        public string Cant { get; set; }
        public string CodPerDurTrat { get; set; }
        public string JustNoPBS { get; set; }
        public string IndRec { get; set; }
        public string EstJM { get; set; }
    }
}
