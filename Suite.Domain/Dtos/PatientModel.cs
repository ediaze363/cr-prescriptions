﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Suite.Domain.Dtos
{
    public class PatientModel
    {
        [JsonProperty("prescripcion")]
        public PrescriptionDto prescripcion { get; set; }
        [JsonProperty("medicamentos")]
        public List<MedicineDto> medicamentos { get; set; }
        [JsonProperty("procedimientos")]
        public List<ProcedureDto> procedimientos { get; set; }
        [JsonProperty("dispositivos")]
        public List<DeviceDto> dispositivos { get; set; }
        [JsonProperty("productosnutricionales")]
        public List<NutritionalProductDto> productosnutricionales { get; set; }
        [JsonProperty("serviciosComplementarios")]
        public List<ComplementaryServiceDto> serviciosComplementarios { get; set; }
    }    
}
