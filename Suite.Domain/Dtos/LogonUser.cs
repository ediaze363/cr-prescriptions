﻿using System;

namespace Suite.Domain.Dtos
{
    public class LogonUser
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Picture { get; set; }
        public string Organization { get; set; }
        public string Email { get; set; }
    }
}
