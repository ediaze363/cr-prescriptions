﻿namespace Suite.Domain.Dtos
{
    public class DeviceDto: BaseDto
    {
        public string ConOrden { get; set; }
        public string TipoPrest { get; set; }
        public string CausaS1 { get; set; }
        public string CodDisp { get; set; }
        public string CanForm { get; set; }
        public string CodFreUso { get; set; }
        public string Cant { get; set; }
        public string CodPerDurTrat { get; set; }
        public string JustNoPBS { get; set; }
        public string IndRec { get; set; }
        public string EstJM { get; set; }
    }
}
