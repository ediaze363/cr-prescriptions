﻿namespace Suite.Domain.Dtos
{
    public class NutritionalProductDto: BaseDto
    {
        public string ConOrden { get; set; }
        public string TipoPrest { get; set; }
        public string CausaS1 { get; set; }
        public string CausaS2 { get; set; }
        public string CausaS3 { get; set; }
        public string CausaS4 { get; set; }
        public string ProNutUtilizado { get; set; }
        public string RznCausaS41 { get; set; }
        public string DescRzn41 { get; set; }
        public string RznCausaS42 { get; set; }
        public string DescRzn42 { get; set; }
        public string CausaS5 { get; set; }
        public string ProNutDescartado { get; set; }
        public string RznCausaS51 { get; set; }
        public string DescRzn51 { get; set; }
        public string RznCausaS52 { get; set; }
        public string DescRzn52 { get; set; }
        public string RznCausaS53 { get; set; }
        public string DescRzn53 { get; set; }
        public string RznCausaS54 { get; set; }
        public string DescRzn54 { get; set; }
        public string TippProNut { get; set; }
        public string DescProdNutr { get; set; }
        public string CodForma { get; set; }
        public string CodViaAdmon { get; set; }
        public string JustNoPBS { get; set; }
        public string Dosis { get; set; }
        public string DosisUM { get; set; }
        public string NoFAdmon { get; set; }
        public string CodFreAdmon { get; set; }
        public string CanTrat { get; set; }
        public string DurTrat { get; set; }
        public string CantTotalF { get; set; }
        public string UFCantTotal { get; set; }
        public string IndRec { get; set; }
        public string NoPrescAso { get; set; }
        public string EstJM { get; set; }
    }
}
