﻿using Newtonsoft.Json;
namespace Suite.Domain.Dtos
{
    public class PrescriptionDto: BaseDto
    {
        public string NoPrescripcion { get; set; }
        public string FPrescripcion { get; set; }
        public string HPrescripcion { get; set; }
        public string CodHabIPS { get; set; }
        public string TipoIDIPS { get; set; }
        public string NroIDIPS { get; set; }
        public string CodDANEMunIPS { get; set; }
        public string DirSedeIPS { get; set; }
        public string TelSedeIPS { get; set; }
        public string TipoIDProf { get; set; }
        public string NumIDProf { get; set; }
        public string PNProfS { get; set; }
        public string SNProfS { get; set; }
        public string PAProfS { get; set; }
        public string SAProfS { get; set; }
        public string RegProfS { get; set; }
        public string TipoIDPaciente { get; set; }
        public string NroIDPaciente { get; set; }
        public string PNPaciente { get; set; }
        public string SNPaciente { get; set; }
        public string PAPaciente { get; set; }
        public string SAPaciente { get; set; }
        public string CodAmbAte { get; set; }
        /* public string EnfHuerfana { get; set; } */
        public string CodEnfHuerfana { get; set; }
        public string CodDxPpal { get; set; }
        public string CodDxRel1 { get; set; }
        public string CodDxRel2 { get; set; }
        public string SopNutricional { get; set; }
        public string CodEPS { get; set; }
        public string TipoIDMadrePaciente { get; set; }
        public string NroIDMadrePaciente { get; set; }
        /* public string TipoTransc { get; set; } */
        public string TipoIDDonanteVivo { get; set; }
        public string NroIDDonanteVivo { get; set; }
        /* public string EstPres { get; set; } */
    }
}
