﻿namespace Suite.Domain.Dtos
{
    public class ProcedureDto: BaseDto
    {
        public string ConOrden { get; set; }
        public string TipoPrest { get; set; }
        public string CausaS11 { get; set; }
        public string CausaS12 { get; set; }
        public string CausaS2 { get; set; }
        public string CausaS3 { get; set; }
        public string CausaS4 { get; set; }
        public string ProPBSUtilizado { get; set; }
        public string CausaS5 { get; set; }
        public string ProPBSDescartado { get; set; }
        public string RznCausaS51 { get; set; }
        public string DescRzn51 { get; set; }
        public string RznCausaS52 { get; set; }
        public string DescRzn52 { get; set; }
        public string CausaS6 { get; set; }
        public string CausaS7 { get; set; }
        public string CodCUPS { get; set; }
        public string CanForm { get; set; }
        public string CodFreUso { get; set; }
        public string Cant { get; set; }
        public string CodPerDurTrat { get; set; }
        public string JustNoPBS { get; set; }
        public string IndRec { get; set; }
        public string EstJM { get; set; }
    }
}
