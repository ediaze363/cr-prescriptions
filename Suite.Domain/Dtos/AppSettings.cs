﻿namespace Suite.Domain.Dtos
{
    public class AppSettings
    {
        /// <summary>
        /// Connection string to the repository database
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Indicates that the password must be encrypted
        /// </summary>
        public bool EncryptPassword { get; set; }
        /// <summary>
        /// Service version
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// WS Prescription for external services
        /// </summary>
        public SourceEndPoint SourceEndPoint { get; set; }

        /// <summary>
        /// The secret key every token will be signed with.
        /// </summary>
        public static string SecretKey
        {
            get { return "253ea3db-a052-48d7-b8d8-75b83a554ff9"; }
        }

        /// <summary>
        /// A password used to generate a key for encryption
        /// </summary>
        public static string SecurityKey
        {
            get { return "e94fd0dd-7d8d-4fde-bcb8-a7f07787d659"; }
        }
    }
}
