﻿namespace Suite.Domain.Dtos
{
    public class PrincipioActivo: BaseDto
    {
        public string ConOrden { get; set; }
        public string CodPriAct { get; set; }
        public string ConcCant { get; set; }
        public string UMedConc { get; set; }
        public string CantCont { get; set; }
        public string UMedCantCont { get; set; }
    }
}
