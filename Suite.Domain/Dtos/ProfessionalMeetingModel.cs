﻿using Newtonsoft.Json;

namespace Suite.Domain.Dtos
{
    public class ProfessionalMeetingModel : BaseDto
    {
        [JsonProperty("junta_profesional")]
        public ProfessionalMeeting Prescription { get; set; }
    }
}
