﻿using System;

namespace Suite.Domain.Dtos
{
    public class ProfessionalMeeting: BaseDto
    {
        public string NoPrescripcion { get; set; }
        public string FPrescripcion { get; set; }
        public string TipoTecnologia { get; set; }
        public int Consecutivo { get; set; }
        public int EstJM { get; set; }
        public object CodEntProc { get; set; }
        public object Observaciones { get; set; }
        public object NoActa { get; set; }
        public object FProceso { get; set; }
        public string TipoIDPaciente { get; set; }
        public string NroIDPaciente { get; set; }
    }
}
