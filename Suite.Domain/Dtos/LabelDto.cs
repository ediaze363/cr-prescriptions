﻿namespace Suite.Domain.Dtos
{
    public class LabelDto
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}
