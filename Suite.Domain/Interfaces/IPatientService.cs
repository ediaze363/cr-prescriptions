﻿using Suite.Domain.Dtos;
using System.Collections.Generic;

namespace Suite.Domain.Interfaces
{
    /// <summary>
    /// Retrieve information about patient prescription
    /// </summary>
    public interface IPatientService
    {
        /// <summary>
        /// Retorna la lista de prescripciones, con la lista de los medicamentos asociados a la prescripción, 
        /// con la lista de procedimientos asociados a la prescripción, la lista de dispositivos médicos asociados a la prescripción, 
        /// con la lista de los productos nutricionales asociados a la prescripción y con la lista de los servicios 
        /// complementarios asociados a la prescripción.
        /// </summary>
        /// <param name="endpoint">EndPoint URL</param>
        /// <param name="date">Query Date</param>
        /// <param name="type">Document Type</param>
        /// <param name="document">Document Number</param>
        /// <returns>Prescription list</returns>
        List<PatientModel> GetPatients(SourceEndPoint endpoint, string date, string type, string document);

        /// <summary>
        /// Retorna el estado de la Junta de Profesionales para la prescripción que se ingresó
        /// </summary>
        /// <param name="endpoint">Endpoint de mipress</param>
        /// <param name="date">fecha de consulta</param>
        /// <param name="type">Tipo de documento del paciente</param>
        /// <param name="document">Número de documento del paciente</param>
        /// <param name="prescriptionId">Número de prescripción</param>
        /// <returns>Lista de profesionales</returns>
        List<ProfessionalMeeting> GetProfessionalMeeting(SourceEndPoint endpoint, string date, string type, string document, string prescriptionId);
    }
}
