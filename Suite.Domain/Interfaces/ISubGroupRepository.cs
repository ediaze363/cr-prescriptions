﻿using Suite.Domain.Entities;
using System.Collections.Generic;

namespace Suite.Domain.Interfaces
{
    public interface ISubGroupRepository
    {
        IList<SubGroup> GetAll(string groupCode);
        IList<SubGroup> GetAll();
    }
}
