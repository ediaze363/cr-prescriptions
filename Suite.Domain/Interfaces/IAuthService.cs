﻿using Suite.Domain.Dtos;

namespace Suite.Domain.Interfaces
{
    public interface IAuthService
    {
        LogonUser Login(string organization, string username, string password);
    }
}
