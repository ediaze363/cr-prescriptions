﻿using Suite.Domain.Entities;
using System.Collections.Generic;

namespace Suite.Domain.Interfaces
{
    public interface IGroupRepository
    {
        IList<Group> GetAll(string term);
    }
}
