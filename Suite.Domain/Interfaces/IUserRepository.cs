﻿using Suite.Domain.Dtos;

namespace Suite.Domain.Interfaces
{
    public interface IUserRepository
    {
        LogonUser Login(string organization, string username, string password);
    }
}
