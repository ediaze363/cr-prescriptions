﻿using System;

namespace Suite.Domain.Entities
{
    public class Group
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
