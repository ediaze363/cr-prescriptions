﻿using System;

namespace Suite.Domain.Entities
{
    public class SubGroup
    {
        public Guid Id { get; set; }
        public Guid GroupId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string GroupCode { get; set; }
        public string Master { get; set; }
    }
}
