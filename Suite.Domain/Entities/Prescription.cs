﻿using System.Runtime.Serialization;

namespace Suite.Domain.Entities
{
    [DataContract]
    public class Prescription
    {
        [DataMember(Name = "NoPrescripcion")]
        public string NoPrescripcion { get; set; }
        [DataMember(Name = "FPrescripcion")]
        public string FPrescripcion
        {
            get{ return (HPrescripcion ?? string.Empty).Substring(0, 8); }
        }
        public string HPrescripcion { get; set; }
        [DataMember(Name = "DocProf")]
        public string DocProf
        {
            get { return $"{TipoIDProf}-{NumIDProf}"; }
        }
        [DataMember(Name = "NomProf")]
        public string ProfName
        {
            get { return $"{PAProfS} {SAProfS}, {PNProfS} {SNProfS}"; }
        }
        [DataMember(Name = "RegProfS")]
        public string RegProfS { get; set; }
        [DataMember(Name = "DocPac")]
        public string DocPac
        {
            get { return $"{TipoIDPaciente}-{NroIDPaciente}"; }
        }
        [DataMember(Name = "NomPaciente")]
        public string NomPaciente
        {
            get { return $"{PAPaciente} {SAPaciente}, {PNPaciente} {SNPaciente}"; }
        }

        // ignored
        public string PNProfS { get; set; }
        public string SNProfS { get; set; }
        public string PAProfS { get; set; }
        public string SAProfS { get; set; }        
        public string PrescriptionDateTime
        {
            get
            {
                var date = PrescriptionDate ?? string.Empty;
                var time = HPrescripcion ?? string.Empty;
                date = date.Length >= 10 ? date.Substring(0, 10) : date;
                time = time.Length >= 8 ? time.Substring(0, 8) : time;
                return $"{date} {time}";
            }
        }
        public string CodHabIPS { get; set; }
        public string CodDANEMunIPS { get; set; }
        public string DirSedeIPS { get; set; }
        public string TelSedeIPS { get; set; }
        public string TipoIDProf { get; set; }
        public string NumIDProf { get; set; }
        public string TipoIDPaciente { get; set; }
        public string NroIDPaciente { get; set; }
        public string PNPaciente { get; set; }
        public string SNPaciente { get; set; }
        public string PAPaciente { get; set; }
        public string SAPaciente { get; set; }
        public string CodAmbAte { get; set; }
        public string EnfHuerfana { get; set; }
        public string CodEnfHuerfana { get; set; }
        public string CodDxPpal { get; set; }
        public string CodDxRel1 { get; set; }
        public string CodDxRel2 { get; set; }
        public string SopNutricional { get; set; }
        public string CodEPS { get; set; }
        public string TipoIDMadrePaciente { get; set; }
        public string NroIDMadrePaciente { get; set; }
        public string TipoTransc { get; set; }
        public string TipoIDDonanteVivo { get; set; }
        public string NroIDDonanteVivo { get; set; }
        public string EstPres { get; set; }
        public string PrescriptionDate { get; set; }
        public string TipoIDIPS { get; set; }
        public string NroIDIPS { get; set; }        
    }
}
